#!/usr/bin/env python3

import tensorflow as tf
import numpy as np
from PIL import Image
from pathlib import Path
from hashlib import sha1


def parse_args():
    import argparse

    arg_parser = argparse.ArgumentParser(
        description="tfrecord extractor",
        formatter_class=argparse.RawTextHelpFormatter)
    arg_parser.add_argument("tfrecord_path", help="Path to tfrecord file.")
    args = arg_parser.parse_args()
    return args


def extractor(data_record):
    features = {
        'input_data/image': tf.FixedLenFeature(shape=[], dtype=tf.string),
        'input_data/image/shape': tf.FixedLenFeature(
            shape=(3,), dtype=tf.int64),
        'labels/labels': tf.FixedLenFeature(shape=(), dtype=tf.string),
        'labels/labels/shape': tf.FixedLenFeature(shape=(1,), dtype=tf.int64),
    }
    sample = tf.parse_single_example(data_record, features)
    return sample


if __name__ == '__main__':
    # Parse args
    args = parse_args()

    # Load dataset
    dataset = tf.data.TFRecordDataset([args.tfrecord_path])

    # Call extractor on dataset
    dataset = dataset.map(extractor)

    # Get iterator
    iterator = dataset.make_one_shot_iterator()
    next_element = iterator.get_next()

    with tf.Session() as sess:
        # Run
        sess.run(tf.global_variables_initializer())

        tfrecord = Path(args.tfrecord_path)
        output_folder = tfrecord.parents[0] / tfrecord.stem
        try:
            output_folder.mkdir(parents=False, exist_ok=False)
        except FileExistsError:
            raise FileExistsError(
                f'Delete {output_folder} first! I\'m to lazy to do that for you :V:'
            )

        try:
            while True:
                # Get data
                data_record = sess.run(next_element)

                # Extract image shape
                image_shape = data_record['input_data/image/shape']

                # Extract image
                image_array = np.fromstring(
                    data_record['input_data/image'], dtype='uint8')
                image_array = np.reshape(image_array,
                                         (image_shape[0], image_shape[1]))
                image = Image.fromarray(image_array)

                # Extract labels shape
                # labels_shape seems to be shape of [number_of_classes]
                labels_shape = data_record['labels/labels/shape']

                # Extract labels
                # labels_array seems to be the class' one hot encoding
                labels_array = np.fromstring(
                    data_record['labels/labels'], dtype='uint8')
                labels_array = np.reshape(labels_array, (*labels_shape, -1))
                # Reduce dimension
                labels_array = np.squeeze(labels_array, axis=1)
                # Convert to list
                labels_list = labels_array.tolist()
                # Index in one hot encoding is class
                class_folder_name = str(labels_list.index(1))
                class_folder = Path(output_folder/class_folder_name)
                class_folder.mkdir(parents=True, exist_ok=True)
                # Make sure its C-contigious to make hashing possible
                if not image_array.flags['C_CONTIGUOUS']:
                    image_array = image_array.ascontigousarray(image_array)
                filename = sha1(image_array).hexdigest()
                output_file_path = class_folder/str(filename + '.png')
                image.save(str(output_file_path))
        except tf.errors.OutOfRangeError:
            print(f'Finished')
